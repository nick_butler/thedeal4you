jQuery(function() {
	jQuery( function() {
		
	// init Isotope
	var $container = jQuery('#container');
	
	$container.imagesLoaded(function () {
		$container.isotope({
			  filter: '.metal'
		});
	});
	// update columnWidth on window resize
	jQuery(window).on('resize', function(){
		$container.isotope('reLayout')
	});
	
	// filter items on button click
	jQuery('#filters').on( 'click', 'button', function() {
		var filterValue = jQuery(this).attr('data-filter');
			$container.isotope({ filter: filterValue });
		});
	
		// AddClass on button click	
		var $optionSets = jQuery('#filters'),
		$optionLinks = $optionSets.find('button');
		$optionLinks.click(function(){
		var $this = jQuery(this);
		if ( $this.hasClass('selected') ) { return false; }
		var $optionSet = $this.parents('#filters');
		$optionSet.find('.selected').removeClass('selected');
		$this.addClass('selected'); 
	    });
	});	
	
	// AddClass on click
	jQuery(document).ready(function(){
		jQuery('.main-navMenu li').click(function(){
			jQuery('.main-navMenu li').removeClass("active");
			jQuery(this).addClass("active");
		});
	});
		
	//Top Search Trigger
    jQuery("#top-search-trigger").click(function(e){
	    jQuery('body').toggleClass('top-search-open');
        jQuery('#cart .shopping-bag').toggleClass('show', false);
        e.preventDefault();
    });
    
	// Top dropdown
	jQuery(".top-dropdown").mouseenter(function() {
			jQuery(this).click();
	});
	jQuery(".top-dropdown").click(function() {
		jQuery(this).addClass('hover');
		jQuery(this).find("ul").stop(true, true).delay(300).fadeIn(300, "easeOutCubic");
	}).mouseleave(function() {
		jQuery(this).find("ul").stop(true, true).delay(300).fadeOut(300, "easeInCubic");
	});
	// Shopping cart dropdown		
	jQuery(".mini-cart").hover(function() {
			jQuery(this).addClass('hover');
			jQuery(".mini-cart .block-content").stop(true, true).delay(300).fadeIn(500, "easeOutCubic");
		}, function() {
			jQuery(".mini-cart .block-content").stop(true, true).delay(300).fadeOut(500, "easeInCubic");
	});
	//Flex slider hover
	jQuery(".hidden-buttons").hover(function() {
		jQuery(".owl-buttons", this).transition({opacity:1});
	}, function() {
		jQuery(".owl-buttons", this).transition({opacity:1}); //should be 0 for buttons to fade out
	});	
	
	// Sort by dropdown
	jQuery(".sorter .sort-by").mouseenter(function() {
			jQuery(this).click();
	});
	jQuery(".sorter .sort-by").click(function() {
		jQuery(this).addClass('hover');
		jQuery(this).find("ul").stop(true, true).delay(300).fadeIn(500, "easeOutCubic");
	}).mouseleave(function() {
		jQuery(this).find("ul").stop(true, true).delay(300).fadeOut(500, "easeInCubic");
	});	
	// Limiter dropdown
	jQuery(".sorter .limiter").mouseenter(function() {
			jQuery(this).click();
	});
	jQuery(".sorter .limiter").click(function() {
		jQuery(this).addClass('hover');
		jQuery(this).find("ul").stop(true, true).delay(300).fadeIn(500, "easeOutCubic");
	}).mouseleave(function() {
		jQuery(this).find("ul").stop(true, true).delay(300).fadeOut(500, "easeInCubic");
	});
	
	//Color box content html popup
	jQuery(".inline-size-guide").colorbox({inline:true, width:"50%",opacity:0.7, close: "<i class=\"fa fa-times\"></i>"});	
	jQuery.each(jQuery('#accordion a.accordion-toggle'), function(i, link){
	    
	        jQuery('#collapse' + 1).collapse({
	            toggle : true,
	            parent : '#accordion'
	        });
	jQuery(link).on('click', 
	    function(){
	        jQuery('#collapse' + 1).collapse('toggle');
	    }
	)
	});
	jQuery('.collapsible').each(function(index) {
			jQuery(this).prepend('<span class="mobile-coll-arrow">+</span>');
			if (jQuery(this).hasClass('active'))
			{
				jQuery(this).children('.block-content').css('display', 'block');
			}
			else
			{
				jQuery(this).children('.block-content').css('display', 'none');
			}			
	});
	
	jQuery('.collapsible .mobile-coll-arrow').click(function() {
			
		var parent = jQuery(this).parent();
		if (parent.hasClass('active'))
		{
			jQuery(this).siblings('.block-content').stop(true).slideUp(300, "easeOutCubic");
			parent.removeClass('active');
			jQuery(this).html('+');
		}
		else
		{
			jQuery(this).siblings('.block-content').stop(true).slideDown(300, "easeOutCubic");
			parent.addClass('active');
			jQuery(this).html('-');
		}
		
	});	
	
	jQuery('.ajaxcart_colorbox').on('mouseenter', function(){
		jQuery(this).colorbox({
			iframe:true,
			opacity:0.8,
			width:"420",
			height:"200",
			opacity:0.7, 
			close: "<i class=\"fa fa-times\"></i>"
		});
	});	

	jQuery('.act-quickview-button').on('mouseenter', function(){
		jQuery(this).colorbox({iframe:true, width:"780px", height:"480px",opacity:0.7, close: "<i class=\"fa fa-times\"></i>"});
	});	
	
   	//Video
   	jQuery(".novaworks-video").colorbox({iframe:true, innerWidth:"99%", innerHeight:"100%",opacity:0.7,close: "<i class=\"fa fa-times\"></i>"});
	// Add (+/-) Button Number Incrementers 
	jQuery(".button-qty").on("click", function() {
	  var $button = jQuery(this);
	  var oldValue = $button.parent().find("input").val();
	
	  if ($button.text() == "+") {
		  var newVal = parseFloat(oldValue) + 1;
		} else {
	   // Don't allow decrementing below zero
	    if (oldValue > 1) {
	      var newVal = parseFloat(oldValue) - 1;
	    } else {
	      newVal = 1;
	    }
	  }
	
	  $button.parent().find("input").val(newVal);
	
	});
	
	// isotope
	$productcontainer = jQuery('.products-grid');
	
	$productcontainer.imagesLoaded(function () {
		$productcontainer.isotope({
		  // options
		  itemSelector: '.item',
			  masonry: {
		     
			  }
			});				
		});
	});
	
	// update columnWidth on window resize
	jQuery(window).on('resize', function(){
		$productcontainer.isotope('reLayout')
	});

	/*
	jQuery('.products-grid').isotope({
	  // options
	  itemSelector: '.item',
		  masonry: {
	     
		  }
		});				
	});
	*/
	
	function setAjaxData(data,iframe){
		if(data.status == 'ERROR'){
			alert(data.message);
		}else{
			if(jQuery('.shopping-bag-item')){
	            jQuery('.shopping-bag-item').replaceWith(data.sidebar);
	        }
			jQuery.colorbox.close();			 					      	        
		}
	}
function ajaxcart(url,id) {
 url += 'isAjax/1';
 url = url.replace("checkout/cart","ajax/index");
 jQuery('.button-ajax-cart-id-'+id).html('<i class="fa fa-circle-o-notch fa-spin"></i>'); 

 try {
        jQuery.ajax( {
            url : url,
            dataType : 'json',
            success : function(data) {
                if(data.status == 'SUCCESS'){   
                	 jQuery('#nova-notification .notification-content').html(data.message);
                	 jQuery('#nova-notification').fadeIn('slow').delay(4000).fadeOut('slow', function() {
					 	jQuery('#nova-notification').fadeOut('slow');
					 }); 
					 jQuery('.button-ajax-cart-id-'+id).html('<i class="fa fa-check"></i>'); 
					 
                }else{
                    bootbox.alert('<p class="error-msg">' + data.message + '</p>');
                }   
                setAjaxData(data,false);
                    
            }
        });
    } catch (e) {
 }
}
function deletecart(url){
		url = url.replace("checkout/cart","ajax/index");
 		try {
                jQuery.ajax( {
                    url : url,
                    dataType : 'json',
                    success : function(data) {  
                        setAjaxData(data,false);   
                    }
                });
            } catch (e) {
 		}
}
function setAjaxCompareData(data,iframe){
		if(data.status == 'ERROR'){
			alert(data.message);
		}else{
			if(jQuery('.block-compare')){
	            jQuery('.block-compare').replaceWith(data.sidebar);
	        }
			if(jQuery('.ajax-compare')){
	            jQuery('.ajax-compare').replaceWith(data.dropdown);
	        }
						 					      	        
		}
}
function ajaxcompare(url,id) {
 url = url.replace("catalog/product_compare/add","ajax/index/compare");
 jQuery('.button-ajax-compare-id-'+id).html('<i class="fa fa-circle-o-notch fa-spin"></i>'); 
 try {
                jQuery.ajax( {
                    url : url,
                    dataType : 'json',
                    success : function(data) {
                        if(data.status == 'SUCCESS'){    
		                	 jQuery('#nova-notification .notification-content').html(data.message);
		                	 jQuery('#nova-notification').fadeIn('slow').delay(4000).fadeOut('slow', function() {
							 	jQuery('#nova-notification').fadeOut('slow');
							 }); 
							 jQuery('.button-ajax-compare-id-'+id).html('<i class="fa fa-check"></i>');                              
                        }else{
                            bootbox.alert('<p class="error-msg">' + data.message + '</p>');
                        }   
                        setAjaxCompareData(data,false);
                            
                    }
                });
            } catch (e) {
 }
}
function removeCompare(url){
	url = url.replace("catalog/product_compare/remove","ajax/index/removecompare"); 
 		try {
                jQuery.ajax( {
                    url : url,
                    dataType : 'json',
                    success : function(data) {  
                        setAjaxCompareData(data,false);   
                    }
                });
            } catch (e) {
 		}
}
function clearCompare(url){
	url = url.replace("catalog/product_compare/clear","ajax/index/clearcompare"); 
 		try {
                jQuery.ajax( {
                    url : url,
                    dataType : 'json',
                    success : function(data) {  
                        setAjaxCompareData(data,false); 
                    }
                });
            } catch (e) {
 		}
}
function ajaxwishlist(url,id) {
 url = url.replace("wishlist/index/add","ajax/index/addwishlist");
 jQuery('.button-ajax-wishlist-id-'+id).html('<i class="fa fa-circle-o-notch fa-spin"></i>'); 
 try {
                jQuery.ajax( {
                    url : url,
                    dataType : 'json',
                    success : function(data) {
                        if(data.status == 'SUCCESS'){  
		                	 jQuery('#nova-notification .notification-content').html(data.message);
		                	 jQuery('#nova-notification').fadeIn('slow').delay(4000).fadeOut('slow', function() {
							 	jQuery('#nova-notification').fadeOut('slow');
							 }); 
							 jQuery('button-ajax-wishlist-id-'+id).html('<i class="fa fa-check"></i>'); 
							if(jQuery('.count-wishlist-box')){
	            				jQuery('#count-wishlist-'+id).replaceWith(data.sidebar);
	            				jQuery('.wishlist-link').replaceWith(data.wishlist_header);
	       					 }                        	  
                        }else{
                            bootbox.alert('<p class="error-msg">' + data.message + '</p>');
                        }   
                            
                    }
                });
            } catch (e) {
 }
}



















